<?php
declare(strict_types=1);

namespace Dungeons\Models;

final class Door
{
    private string $type;
    private string $description;
    private int $left;
    private int $top;
    private int $width;
    private int $height;
    private string $to;
    private ?string $image;

    final public function __construct(string $type, string $description, int $left, int $top, int $width, int $height, string $to, ?string $image)
    {
        $this->type        = $type;
        $this->description = $description;
        $this->left        = $left;
        $this->top         = $top;
        $this->width       = $width;
        $this->height      = $height;
        $this->to          = $to;
        $this->image       = $image;
    }

    final public function getHtml(bool $includeLink = false, int $size = 10): string
    {
        switch ($this->type) {
            case 'open':
                $color = '#FFFF00';
                break;
            case 'unlocked':
                $color = '#FF9900';
                break;
            case 'locked':
                $color = '#FF0000';
                break;
            case 'hidden':
                $color = '#FF00FF';
                break;
            default:
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \Exception('Invalid Door Type');
        }
        ob_start();
        if ($this->image !== null) {
            if ($this->width === 0) {
                $rotation  = 0;
                $width     = $size;
                $height    = $this->height * $size;
                $top       = $this->top * $size;
                $left      = ($this->left * $size);
                $translate = 0;
            } else {
                $height    = $this->width * $size;
                $width     = $size;
                $left      = ($this->top * $size);
                $top       = $this->left * $size;
                $rotation  = 90;
                $translate = -($height + ($top * 2));
            }
            ?>
            <!--suppress HtmlUnknownAttribute, HtmlDeprecatedTag -->
            <image
                    xlink:href="<?= $this->image ?>"
                    x="<?= $left ?>"
                    transform="rotate(<?= $rotation ?>) translate(0, <?= $translate ?>)"
                    y="<?= $top ?>"
                    width="<?= $width ?>"
                    height="<?= $height ?>"
                    data-tippy-content="<?= htmlspecialchars($this->description) ?>"
            ></image>
            <?php
        } else {
            if ($this->width === 0) {
                $width  = ($size * 2 / 5);
                $height = $this->height * $size;
                $top    = $this->top * $size;
                $left   = ($this->left * $size) - ($size / 5);
            } else {
                $width  = $this->width * $size;
                $height = ($size * 2 / 5);
                $top    = ($this->top * $size) - ($size / 5);
                $left   = $this->left * $size;
            }
            ?>
            <rect
                    data-tippy-content="<?= htmlspecialchars($this->description) ?>"
                    x="<?= $left ?>"
                    y="<?= $top ?>"
                    width="<?= $width ?: 2 ?>"
                    height="<?= $height ?: 2 ?>"
                    stroke-width="1"
                    stroke="<?= $color ?>"
                    fill="<?= $color ?>"
            ></rect>
            <?php
        }
        $door = ob_get_clean();
        if ($includeLink) {
            ob_start();
            ?>
            <a href="javascript:void(0);" class="js_doors" data-target-room="<?= htmlspecialchars($this->to) ?>">
                <?= $door ?>
            </a>
            <?php
            $door = ob_get_clean();
        }
        return $door;
    }
}
