<?php
declare(strict_types=1);

namespace Dungeons\Models;

final class Treasure
{
    private string $description;
    private int $left;
    private int $top;

    final public function __construct(string $description, int $left, int $top)
    {
        $this->description = $description;
        $this->left        = $left;
        $this->top         = $top;
    }

    final public function getHtml(int $size = 50): string
    {
        ob_start();
        ?>
        <rect
                data-tippy-content="<?= htmlspecialchars($this->description) ?>"
                x="<?= ($this->left + 0.25) * $size ?>"
                y="<?= ($this->top + 0.25) * $size ?>"
                width="<?= 0.5 * $size ?>"
                height="<?= 0.5 * $size ?>"
                stroke-width="1"
                stroke="#00FF00"
                fill="#00FF00"
        ></rect>
        <?php
        return ob_get_clean();
    }
}
