<?php
declare(strict_types=1);

namespace Dungeons\Models;

final class Dungeon
{
    /** @var array|Room[] */
    private array $rooms;
    private string $extraHtml;

    final public function __construct(Room ...$rooms)
    {
        $this->rooms = $rooms;
    }

    final public static function fromDirectory(string $map): Dungeon
    {
        $rooms    = [];
        $baseJson = json_decode(file_get_contents(realpath('../Dungeons') . DIRECTORY_SEPARATOR . 'base.json'), true);
        $json     = json_decode(file_get_contents(realpath('../Dungeons') . DIRECTORY_SEPARATOR . $map . DIRECTORY_SEPARATOR . 'customizations.json'), true);
        foreach ($json as $roomId => $extraData) {
            if (file_exists(realpath('../Dungeons') . DIRECTORY_SEPARATOR . $map . DIRECTORY_SEPARATOR . $roomId . '.json')) {
                $extraData += json_decode(file_get_contents(realpath('../Dungeons') . DIRECTORY_SEPARATOR . $map . DIRECTORY_SEPARATOR . $roomId . '.json'), true);
            }
            if (file_exists(realpath('../Dungeons') . DIRECTORY_SEPARATOR . $map . DIRECTORY_SEPARATOR . $roomId . '.html')) {
                $extraData['detailsHtml'] = file_get_contents(realpath('../Dungeons') . DIRECTORY_SEPARATOR . $map . DIRECTORY_SEPARATOR . $roomId . '.html');
            }
            $doors = [];
            foreach (($extraData['doors'] ?? []) + ($baseJson[$roomId]['doors'] ?? []) as $to => $extraDoorData) {
                $doors[$to] = $extraDoorData + ($baseJson[$roomId]['doors'][$to] ?? []);
            }
            $extraData['doors'] = $doors;
            $rooms[]            = Room::fromData((string)$roomId, $extraData + ($baseJson[$roomId] ?? []));
        }
        $dungeon = new self(...$rooms);
        if (file_exists(realpath('../Dungeons') . DIRECTORY_SEPARATOR . $map . DIRECTORY_SEPARATOR . 'dungeon.html')) {
            $dungeon->setExtraHtml(file_get_contents(realpath('../Dungeons') . DIRECTORY_SEPARATOR . $map . DIRECTORY_SEPARATOR . 'dungeon.html'));
        }
        return $dungeon;
    }

    final public function setExtraHtml(string $extraHtml): Dungeon
    {
        $this->extraHtml = $extraHtml;
        return $this;
    }

    final public function getHtml(): string
    {
        $height = 0;
        $width  = 0;
        foreach ($this->rooms as $room) {
            $height = max($height, $room->getHeight() + $room->getTop());
            $width  = max($width, $room->getWidth() + $room->getLeft());
        }
        ob_start();
        ?>
        <div class="container">
            <div class="row">
                <div class="col s12 m8">
                    <div class="dungeon" style="height: <?= $height ?>px; width: <?= $width ?>px;">
                        <?php
                        foreach ($this->rooms as $room) {
                            echo $room->getHtml();
                        }
                        ?>
                        <div style="clear: both;"></div>
                    </div>
                </div>
                <div class="col s12 m4">
                    <?php
                    if (isset($this->extraHtml)) {
                        ?><p><?= $this->extraHtml ?></p><?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }
}
