<?php
declare(strict_types=1);

namespace Dungeons\Models;

final class Monster
{
    private string $name;
    private int $left;
    private int $top;
    private int $size;
    private string $imageUrl;

    final public function __construct(string $name, int $left, int $top, int $size, string $imageUrl)
    {
        $this->name     = $name;
        $this->left     = $left;
        $this->top      = $top;
        $this->size     = $size;
        $this->imageUrl = $imageUrl;
    }

    final public static function fromData(string $monsterName, array $data, array $position): Monster
    {
        $filePath = realpath('../Monsters') . DIRECTORY_SEPARATOR . $monsterName . '.json';
        if (file_exists($filePath)) {
            $data = $data + json_decode(file_get_contents($filePath), true);
        }
        return new Monster($monsterName, $position['left'], $position['top'], $data['size'], $data['imageUrl']);
    }

    final public function getListHtml(): string
    {
        ob_start();
        ?>
        <li class="collection-item avatar" style="min-height: 62px;">
            <img src="<?= $this->imageUrl ?>" class="circle" alt="thumb"/>
            <span class="title"><?= $this->name ?></span>
        </li>
        <?php
        return ob_get_clean();
    }

    final public function getImageHtml(int $size = 50): string
    {
        $pathId = uniqid();
        $left = $this->left * $size;
        $top = $this->top * $size;
        $size = $this->size * $size;
        ob_start();
        ?>
        <defs>
            <clipPath id="<?= $pathId ?>">
                <circle cx="<?= $left + $size / 2 ?>" cy="<?= $top + ($size / 2) ?>" r="<?= $size / 2 ?>" fill="#FFFFFF"></circle>
            </clipPath>
        </defs>
        <!--suppress HtmlUnknownAttribute, HtmlDeprecatedTag -->
        <image xlink:href="<?= $this->imageUrl ?>" clip-path="url(#<?= $pathId ?>)" x="<?= $this->left * 50 ?>" y="<?= $this->top * 50 ?>" width="<?= $this->size * 50 ?>" height="<?= $this->size * 50 ?>"></image>
        <?php
        return ob_get_clean();
    }
}
