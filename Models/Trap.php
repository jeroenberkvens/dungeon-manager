<?php
declare(strict_types=1);

namespace Dungeons\Models;

final class Trap
{
    private string $description;
    private int $left;
    private int $top;
    private int $width;
    private int $height;

    final public function __construct(string $description, int $left, int $top, int $width = 1, int $height = 1)
    {
        $this->description = $description;
        $this->left        = $left;
        $this->top         = $top;
        $this->width       = $width;
        $this->height      = $height;
    }

    final public function getHtml(int $size = 50): string
    {
        ob_start();
        ?>
        <rect
                data-tippy-content="<?= htmlspecialchars($this->description) ?>"
                x="<?= ($this->left + 0.25) * $size ?>"
                y="<?= ($this->top + 0.25) * $size ?>"
                width="<?= ($this->width - 0.5) * $size ?>"
                height="<?= ($this->height - 0.5) * $size ?>"
                stroke-width="1"
                stroke="#FF0000"
                fill="#FF0000"
        ></rect>
        <?php
        return ob_get_clean();
    }
}
