<?php
declare(strict_types=1);

namespace Dungeons\Models;

final class Encounter
{
    private string $url;
    private string $name;
    private array $monsters;
    private string $notesHtml = '';

    final public function __construct(string $url, string $name, Monster ...$monsters)
    {
        $this->url      = $url;
        $this->name     = $name;
        $this->monsters = $monsters;
    }

    final public static function fromData(array $data): Encounter
    {
        $monsters = [];
        if (array_key_exists('monsters', $data)) {
            foreach ($data['monsters'] as $monsterName => $monsterData) {
                foreach ($monsterData['positions'] as $position) {
                    $monsters[] = Monster::fromData($monsterName, $monsterData, $position);
                }
            }
        }
        $encounter = new self($data['url'], $data['name'], ...$monsters);
        if (array_key_exists('notesHtml', $data)) {
            $encounter->setNotesHtml($data['notesHtml']);
        }
        return $encounter;
    }

    final public function getTitleHtml(): string
    {
        ob_start();
        ?><a href="<?= $this->url ?>" target="_blank"><?= $this->name ?: $this->url ?></a><?php
        return ob_get_clean();
    }

    final public function getDetailsHtml(): string
    {
        ob_start();
        ?>
        <ul class="collection">
            <?php
            foreach ($this->monsters as $monster) {
                echo $monster->getListHtml();
            }
            ?>
        </ul>
        <?php
        return ob_get_clean() . $this->notesHtml;
    }

    final public function getMonsterMapHtml(): string
    {
        $monstersHtml = '';
        foreach ($this->monsters as $monster) {
            $monstersHtml .= $monster->getImageHtml();
        }
        return $monstersHtml;
    }

    final private function setNotesHtml(string $notesHtml): Encounter
    {
        $this->notesHtml = $notesHtml;
        return $this;
    }
}
