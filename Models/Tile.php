<?php
declare(strict_types=1);

namespace Dungeons\Models;

final class Tile
{
    private int $left;
    private int $top;
    private int $rotation;
    private int $width;
    private int $height;
    private string $side;
    private string $name;

    final public function __construct(int $left, int $top, int $rotation, int $width, int $height, string $side, string $name)
    {
        $this->left     = $left;
        $this->top      = $top;
        $this->rotation = $rotation;
        $this->width    = $width;
        $this->height   = $height;
        $this->side     = $side;
        $this->name     = $name;
    }

    final public function getHtml(int $size = 50): string
    {
        $url = implode(
            DIRECTORY_SEPARATOR,
            [
                'images',
                $this->side,
                $this->width . 'x' . $this->height,
                $this->name,
            ]
        );
        switch ($this->rotation) {
            case 90:
                $transform = 'rotate(90) translate('.(($this->top) * $size).', -' . (($this->left + $this->height) * $size) . ')';
                break;
            case 180:
                $transform = 'rotate(180) translate(-' . (($this->width + $this->left) * $size) . ', -' . (($this->height + $this->top) * $size) . ')';
                break;
            case 270:
                $transform = 'rotate(270) translate(-'.(($this->top + $this->width) * $size).', ' . ($this->left * $size) . ')';
                break;
            default:
                $transform = 'translate('.($this->left * $size).', '.($this->top * $size).')';
        }
        ob_start();
        ?>
        <!--suppress HtmlUnknownAttribute, HtmlDeprecatedTag -->
        <image xlink:href="<?= $url ?>" width="<?= $this->width * $size ?>" height="<?= $this->height * $size ?>" transform="<?= $transform ?>"></image>
        <?php
        return ob_get_clean();
    }
}
