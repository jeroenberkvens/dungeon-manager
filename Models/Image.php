<?php
declare(strict_types=1);

namespace Dungeons\Models;

final class Image
{
    private array $data;

    final public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getWidth(): int
    {
        return (max(array_column($this->data, 2)) + 1) * 10;
    }

    public function getHeight(): int
    {
        return (max(array_column($this->data, 3)) + 1) * 10;
    }

    final public function getHtml(int $size = 10, callable $callback = null): string
    {
        $maxX = max(array_column($this->data, 2)) + 1;
        $maxY = max(array_column($this->data, 3)) + 1;
        ob_start();
        ?>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 <?= $maxX * $size ?> <?= $maxY * $size ?>" style="min-width: <?= $maxX * 10 ?>px;">
            <?php
            ob_start();
            foreach ($this->data as $data) {
                if (count($data) === 6) {
                    [$startX, $startY, $endX, $endY, $side, $name] = $data;
                    $width = ($endX - $startX) + 1;
                    $height = ($endY - $startY) + 1;
                    $url = implode(
                        DIRECTORY_SEPARATOR,
                        [
                            'images',
                            $side,
                            min($width, $height) . 'x' . max($width, $height),
                            $name,
                        ]
                    );
                    ?>
                    <!--suppress HtmlUnknownAttribute, HtmlDeprecatedTag -->
                    <image xlink:href="<?= $url ?>" x="<?= $startX * $size ?>" y="<?= $startY * $size ?>" width="<?= $width * $size ?>" height="<?= $height * $size ?>"></image>
                    <?php
                } else {
                    [$startX, $startY, $endX, $endY] = $data;
                    for ($x = $startX; $x <= $endX; $x++) {
                        for ($y = $startY; $y <= $endY; $y++) {
                            ?>
                            <rect x="<?= $x * $size ?>" y="<?= $y * $size ?>" width="<?= $size ?>" height="<?= $size ?>" stroke-width="1" stroke="#000000" fill="#FFFFFF"/>
                            <?php
                        }
                    }
                }
            }
            echo $callback !== null ? $callback(ob_get_clean()) : ob_get_clean();
            ?>
        </svg>
        <?php
        return ob_get_clean();
    }
}
