<?php
declare(strict_types=1);

namespace Dungeons\Models;

final class Room
{
    private string $id;
    private int $left;
    private int $top;
    private Image $image;
    /** @var array|Door[] */
    private array $doors;
    /** @var array|OrdinaryObject[] */
    private array $objects;
    /** @var array|Treasure[] */
    private array $treasures;
    /** @var array|Trap[] */
    private array $traps;
    /** @var array|Tile[] */
    private array $tiles;
    private ?Encounter $encounter;
    private string $detailsHtml;

    final public function __construct(string $id, int $left, int $top, Image $image, array $doors, array $objects, array $treasures, array $traps, array $tiles, Encounter $encounter = null)
    {
        $this->id        = $id;
        $this->left      = $left;
        $this->top       = $top;
        $this->image     = $image;
        $this->doors     = $doors;
        $this->objects   = $objects;
        $this->treasures = $treasures;
        $this->traps     = $traps;
        $this->tiles = $tiles;
        $this->encounter = $encounter;
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->validateDoors();
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->validateTreasures();
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->validateTraps();
    }

    /**
     * @throws \Exception
     */
    final private function validateDoors(): void
    {
        foreach ($this->doors as $door) {
            if (!($door instanceof Door)) {
                throw new \Exception('This door is not a valid Door');
            }
        }
    }

    /**
     * @throws \Exception
     */
    final private function validateTreasures(): void
    {
        foreach ($this->treasures as $treasure) {
            if (!($treasure instanceof Treasure)) {
                throw new \Exception('This treasure is not a valid Treasure');
            }
        }
    }

    /**
     * @throws \Exception
     */
    final private function validateTraps(): void
    {
        foreach ($this->traps as $trap) {
            if (!($trap instanceof Trap)) {
                throw new \Exception('This trap is not a valid Trap');
            }
        }
    }

    final public static function fromData(string $roomId, array $data): Room
    {
        $encounter = array_key_exists('encounter', $data) ? Encounter::fromData($data['encounter']) : null;
        $doors     = [];
        if (array_key_exists('doors', $data)) {
            foreach ($data['doors'] as $to => $door) {
                $doors[] = new Door($door['type'], $door['description'] ?? '', $door['position'][0], $door['position'][1], $door['size'][0], $door['size'][1], (string)$to, $door['image'] ?? null);
            }
        }
        $objects = [];
        if (array_key_exists('objects', $data)) {
            foreach ($data['objects'] as $object) {
                $objects[] = new OrdinaryObject($object['description'] ?? null, $object['position'][0], $object['position'][1]);
            }
        }
        $treasures = [];
        if (array_key_exists('treasure', $data)) {
            foreach ($data['treasure'] as $treasure) {
                $treasures[] = new Treasure($treasure['description'], $treasure['position'][0], $treasure['position'][1]);
            }
        }
        $traps = [];
        if (array_key_exists('traps', $data)) {
            foreach ($data['traps'] as $trap) {
                $traps[] = new Trap($trap['description'], $trap['position'][0], $trap['position'][1], $trap['size'][0] ?? 1, $trap['size'][1] ?? 1);
            }
        }
        $tiles = [];
        if (array_key_exists('tiles', $data)) {
            foreach ($data['tiles'] as $tile) {
                $tiles[] = new Tile($tile['position'][0], $tile['position'][1], $tile['rotation'] ?? 0, $tile['size'][0], $tile['size'][1], $tile['side'], $tile['name']);
            }
        }
        $image = new Image($data['image']);
        $room  = new Room($roomId, $data['left'], $data['top'], $image, $doors, $objects, $treasures, $traps, $tiles, $encounter);
        if (array_key_exists('detailsHtml', $data)) {
            $room->setDetailsHtml($data['detailsHtml']);
        }
        return $room;
    }

    final public function setDetailsHtml(string $detailsHtml): Room
    {
        $this->detailsHtml = $detailsHtml;
        return $this;
    }

    final public function getTop(): int
    {
        return $this->top;
    }

    final public function getLeft(): int
    {
        return $this->left;
    }

    final public function getWidth(): int
    {
        return $this->image->getWidth();
    }

    final public function getHeight(): int
    {
        return $this->image->getHeight();
    }

    final public function getHtml(): string
    {
        ob_start();
        ?>
        <div class="room" style="top: <?= $this->top ?>px; left: <?= $this->left ?>px;">
            <?= $this->image->getHtml(
                10,
                function (string $imageHtml) {
                    ob_start();
                    echo $this->getLinkHtml($imageHtml, $this->id);
                    foreach ($this->doors as $door) {
                        echo $door->getHtml();
                    }
                    return ob_get_clean();
                }
            ) ?>
            <?= $this->getRoomModalHtml($this->id) ?>
        </div>
        <?php
        return ob_get_clean();
    }

    final private function getLinkHtml(string $imageHtml, string $roomId)
    {
        ob_start();
        ?><a href="#room-<?= $roomId ?>" class="modal-trigger" style="pointer-events: fill;"><?= $imageHtml ?></a><?php
        return ob_get_clean();
    }

    final public function getRoomModalHtml(string $id): string
    {
        ob_start();
        ?>
        <div id="room-<?= $id ?>" class="modal">
            <div class="modal-content">
                <?php
                if ($this->encounter !== null) {
                    ?><h2 class="center"><?= $this->encounter->getTitleHtml() ?></h2><?php
                }
                ?>
                <div class="row">
                    <div class="col s8">
                        <?= $this->image->getHtml(
                            50,
                            function (string $imageHtml) {
                                ob_start();
                                echo $imageHtml;
                                foreach ($this->tiles as $tile) {
                                    echo $tile->getHtml();
                                }
                                foreach ($this->traps as $trap) {
                                    echo $trap->getHtml();
                                }
                                foreach ($this->doors as $door) {
                                    echo $door->getHtml(true, 50);
                                }
                                if ($this->encounter !== null) {
                                    echo $this->encounter->getMonsterMapHtml();
                                }
                                foreach ($this->objects as $object) {
                                    echo $object->getHtml();
                                }
                                foreach ($this->treasures as $treasure) {
                                    echo $treasure->getHtml();
                                }
                            }
                        ) ?>
                    </div>
                    <?php
                    if ($this->encounter !== null) {
                        echo $this->encounter->getDetailsHtml();
                    }
                    if (isset($this->detailsHtml)) {
                        echo $this->detailsHtml;
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }
}
