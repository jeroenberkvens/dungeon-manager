import $ from "jquery";
import Modal from "materialize-css";
import tippy from 'tippy.js';

(function () {
    Modal.AutoInit();
    document.addEventListener('DOMContentLoaded', function () {
        M.Modal.init(document.querySelectorAll('.modal'));
    });
    $('.js_doors').on('click', function () {
        const targetRoom = $(this).data('targetRoom');
        const $targetRoom = $('#room-' + targetRoom);
        const currentRoomInstance = M.Modal.getInstance($(this).closest('.modal'));
        currentRoomInstance.close();
        const targetRoomInstance = M.Modal.getInstance($targetRoom[0]);
        targetRoomInstance.open();
    });
    tippy('[data-tippy-content]');
})();
