<?php

use Dungeons\Models\Dungeon;
use Dungeons\Models\Encounter;
use Dungeons\Models\Monster;
use Dungeons\Models\Room;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$basePath = realpath('..');
spl_autoload_register(
    function ($className) use ($basePath) {
        $classNameParts = explode('\\', $className);
        array_shift($classNameParts);
        /** @noinspection PhpIncludeInspection */
        include $basePath . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $classNameParts) . '.php';
    }
);

if (!isset($_GET['map'])) {
    require_once 'dungeons.php';
    return;
}

$dungeon = Dungeon::fromDirectory($_GET['map']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Base Dungeon</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection"/>
</head>
<body>
<?= $dungeon->getHtml(); ?>
<script type="text/javascript" src="js/app.js"></script>
</body>
</html>
