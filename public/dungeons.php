<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Select Dungeon</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection"/>
</head>
<body>
<div class="container">
    <div class="section">
        <h1>Dungeons</h1>
        <div class="collection">
            <?php
            foreach (scandir(realpath('../Dungeons')) as $item) {
                if (is_dir(realpath('../Dungeons') . DIRECTORY_SEPARATOR . $item) && substr($item, 0, 1) !== '.') {
                    ?><a href="?map=<?= htmlspecialchars($item) ?>" class="collection-item"><?= htmlspecialchars($item) ?></a><?php
                }
            }
            ?>
        </div>
    </div>
</div>
</body>
